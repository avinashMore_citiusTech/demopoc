import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PatientListComponent } from './patient-list/patient-list.component';
import { PatientAddComponent } from './patient-add/patient-add.component';
import { PatientEditComponent } from './patient-edit/patient-edit.component';
import { PatientDetailsComponent } from './patient-details/patient-details.component';
import { PatientAddGuard } from '../guard/patient-add/patient-add.guard';

const routes: Routes = [{
  path: '', 
  redirectTo: 'allpatients', 
  pathMatch:'full'
},{
  path: 'allpatients', 
  component: PatientListComponent
}, {
  path: 'addpatient', 
  component: PatientAddComponent,
  canActivate: [PatientAddGuard ]
}, {
  path: 'patient/edit/:id', 
  component: PatientEditComponent
}, {
  path: 'patient/:id', 
  component: PatientDetailsComponent
}, {
  path: '**', 
  redirectTo: 'allpatients'
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [PatientAddGuard]
})
export class PatientRoutingModule { }

export const PatientComponent = [
  PatientListComponent,
  PatientDetailsComponent,
  PatientEditComponent,
  PatientAddComponent
];
