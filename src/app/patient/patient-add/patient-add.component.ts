import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Patient } from '../../model/patient';
import { PatientService } from '../patient.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-patient-add',
  templateUrl: './patient-add.component.html',
  styleUrls: ['./patient-add.component.scss']
})
export class PatientAddComponent implements OnInit {

 public patientDetail: Patient = new Patient();

  constructor(
    private _router: Router, 
    private _patientService : PatientService, 
    private _toastr : ToastrService
  ) { }

  ngOnInit() {
  }

  onGoBack() {
    this._router.navigate(["/allpatients"]);
  }

  onSubmit(patientDetail) {
    this._patientService.addPatient(patientDetail).subscribe(({id}) => {
      this._toastr.success("Patient Added sucessfully");
      this._router.navigate(["/patient", id])
    })
  }
}
