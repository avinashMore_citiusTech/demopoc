import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { Patient } from '../../model/patient';
import { AgePipe } from '../../pipe/age/age.pipe';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-patient-main',
  templateUrl: './patient-main.component.html',
  styleUrls: ['./patient-main.component.scss']
})
export class PatientMainComponent implements OnInit, OnChanges {
  @Input() patientDetail: Patient;
  @Input() title: string;
  @Output() onSubmit = new EventEmitter();
  @Output() onCancel = new EventEmitter();
  public formSubmitted: BehaviorSubject<boolean> = new BehaviorSubject(false);

  public patientForm: FormGroup;
  public errorMessages: object;

  constructor( private _agePipe: AgePipe, public fb: FormBuilder) { }
  ngOnInit() {
    this.patientForm = this._getForm()
    this.errorMessages =this._getErrorMessages();
  }

  _getForm() {
  return this.fb.group({
      patientName: ['', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(30),
        Validators.pattern('^[a-zA-Z_ ]*$')
      ]],
      dateOfBirth: [null, [
        Validators.required
      ]],
      age:['', [
        Validators.required,
        Validators.minLength(1),
        Validators.maxLength(3),
        Validators.pattern('^(0|[1-9][0-9]*)$')
      ]]
    }, {updateOn: 'blur'});
  }

  _getErrorMessages() {
    return {
      patientName: {
        required: 'Please enter patient name',
        minlength: 'Please enter minimum 3 character',
        maxlength: 'Please enter maximum 30 character',
        pattern: "Plese enter valid name"
      },
      dateOfBirth: {
        required: 'Please enter date of birth'
      },
      age: {
        required: 'Please enter age',
        minlength: 'Please enter minimum 1 number',
        maxlength: 'age shold not greater than 3 number',
        pattern: "please enter valid number"
      }
    }
  }



  onChnageDate(dateOfBirth) {
    if(dateOfBirth) {
      const age = this._agePipe.transform(dateOfBirth).toString();
      this.patientForm.patchValue({dateOfBirth, age})
    }
  }

  onSubmitMain() {
    this.formSubmitted.next(true);
    if(this.patientForm.valid) {
      const details = {... this.patientForm.value, deletedDate: null}
      this.onSubmit.emit(details);
    }
  }

  onCancelMain() {
    this.onCancel.emit();
  }

  ngOnChanges(changes) {
    if(changes.patientDetail.currentValue && changes.patientDetail.currentValue.patientName) {
      const patient = changes.patientDetail.currentValue;
      this.patientForm.setValue({
        patientName: patient.patientName,
        dateOfBirth: new Date(patient.dateOfBirth),
        age: patient.age
      });
    }
  }

}
