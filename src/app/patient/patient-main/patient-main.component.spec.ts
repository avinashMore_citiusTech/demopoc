import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientMainComponent } from './patient-main.component';
import { FormsModule, ReactiveFormsModule, FormBuilder } from '@angular/forms';

describe('PatientMainComponent', () => {
  let component: PatientMainComponent;
  let fixture: ComponentFixture<PatientMainComponent>;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientMainComponent ],
      imports: [FormsModule, ReactiveFormsModule],
      providers: [{ provide: FormBuilder, useValue: formBuilder }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
