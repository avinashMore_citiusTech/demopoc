import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'

import { PatientRoutingModule, PatientComponent } from './patient-routing.module';
import { PatientService } from './patient.service';
import { CommonComponentModule } from '../common-component/common-component.module';
import { AgeSortPipe } from '../pipe/age-sort/age-sort.pipe';
import { ModalService } from '../common-component/modal/modal.service';
import { PatientMainComponent } from './patient-main/patient-main.component';
import { ApiService } from '../common-component/api/api.service';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import {StoreModule} from '@ngrx/store'


@NgModule({
  imports: [
    CommonModule,
    PatientRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    CommonComponentModule,
    TooltipModule.forRoot()
  ],
  declarations: [
    PatientComponent,
    AgeSortPipe,
    PatientMainComponent
  ],
  exports: [PatientMainComponent],
  providers: [PatientService, ModalService, ApiService]
})
export class PatientModule { }
