import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patient.service';
import { Router } from '@angular/router';
import { Patient } from 'src/app/model/patient';
import { Observable } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {
  public patientList: Observable<any>;
  public patients: Patient[];
  public patientIdMain;
  public ageSortorder: string;
  public sortOrder: string;
  public isFilter: boolean = false;
  public patientRecentlyDeleted: Patient[] = [];
  public setInveral;
  public isMultipleDeletePatient;
  public searchString: string;

  constructor(private _patientService: PatientService, private _router: Router) {

    this.patientList = Observable.create((observer: any) => {
      // Runs on every search
      observer.next(this.searchString);
    })
      .pipe(
        mergeMap((token: string) =>  this._patientService.getSearchData(token))
      );
   }

  ngOnInit() {
    this._getAllPatients();
  }

  onAutoCompleteSelectPatient({id}) {
    this._router.navigate(["/patient", id]);
  }

  onFilterByName() {
    this._getAllPatients()
  }

  _getAllPatients() {
    if(this.searchString) {
      this.isFilter = true;
    }
    this._patientService.getAllPatient(this.searchString).subscribe((result) => {
      debugger;
      this.patients = result.filter((value)=> value.deletedDate == null);
      this.patientRecentlyDeleted = result.filter((value)=> value.deletedDate !== null);
      if(this.patientRecentlyDeleted.length > 0) {
        this.setInveral = this._setInterval();
      } else {
        this._clearInterval();
      }
    });
  }

  onAddPatient() {
    this._router.navigate(["/addpatient"]);
  }

  onUndo(patient) {
     this._patientService.restorePatient(patient).subscribe((value) => {
       this._getAllPatients();
    });
  }

  onEditPatient(patientId) {
    this._router.navigate(["/patient/edit/", patientId]);
  }

  onViewPatient(patientId) {
    this._router.navigate(["/patient", patientId]);
  }

  onDeletePatient(patient) {
    this._patientService.deletePatient(patient).subscribe((value)=> {
      this._getAllPatients();
      this.setInveral = this._setInterval(); 
    }); 
  }

  onDeletePatients() {
    const deletedPatients = this.patients.reduce((deletedPatients, value) => {
      if(value.isDelete === true) {
        deletedPatients.push(value);
      }
      return deletedPatients;
    }, []);

    this._patientService.deleteMulitplePatients(deletedPatients).subscribe((value) => {
      this.isMultipleDeletePatient = false;
      this._getAllPatients();
    })
  }

  _checkAndDeleteRecentlyDeletedPatient() {
    if(this.patientRecentlyDeleted.length > 0) {
      const deletedPatientsIds =  this.patientRecentlyDeleted.reduce((deletedPatientsIds, value) => {
        if(new Date(value.deletedDate) <= new Date()) {
          deletedPatientsIds.push(value.id); 
        }
        return deletedPatientsIds;
      }, []);
      deletedPatientsIds.forEach((value) => {
        this._patientService.deletedpatientPermantly(value).subscribe((result)=> {
          this._getAllPatients();
        });
      })
    } else {
      this._clearInterval()
    }
  }

  _setInterval() {
   return setInterval(() => {
      this._checkAndDeleteRecentlyDeletedPatient();
    }, 1000);
  }

  _clearInterval() {
    clearInterval(this.setInveral);
  }

}
