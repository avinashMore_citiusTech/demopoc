import { Component, OnInit } from '@angular/core';
import { PatientService } from '../patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-patient-edit',
  templateUrl: './patient-edit.component.html',
  styleUrls: ['./patient-edit.component.scss']
})
export class PatientEditComponent implements OnInit {
  public patientDetail;
  public patientId;
  constructor(private _router: Router, private _patientService: PatientService, private _activatedRoute: ActivatedRoute, private _toastr: ToastrService) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe( params => {
      this.patientId = params['id'];
      this._patientService.getPatientDetail(this.patientId).subscribe((patientDetails) => {
        this.patientDetail = patientDetails;
      });
    })
  }

  onCancel() {
    this._router.navigate(["/allpatients"]);
  }
  onSubmit(patientDetail) {
    this._patientService.updatePatient(this.patientId, patientDetail).subscribe((result)=> {
        this._toastr.success("Patient updated sucessfully");
      this._router.navigate(["/patient", result])
    })
    // this._patientService.addPatient(this.patientId, patientDetail).then(({patientId}) => {
  
    // })
  }

}
