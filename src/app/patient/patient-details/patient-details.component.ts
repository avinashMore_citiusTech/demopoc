import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { PatientService } from '../patient.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-patient-details',
  templateUrl: './patient-details.component.html',
  styleUrls: ['./patient-details.component.scss']
})
export class PatientDetailsComponent implements OnInit {
  public patientDetail;
  public patientId;
  constructor(
    private _router: Router, 
    private _patientService: PatientService, 
    private _activatedRoute: ActivatedRoute,
    private _toastr: ToastrService) { }

  ngOnInit() {
    this._activatedRoute.params.subscribe( params => {
       const patientId = params['id'];

      this._patientService.getPatientDetail(patientId).subscribe((result)=> {
        this.patientDetail = result;
      }, (error) => {
        if(error.errorResponse.status === 404) {
          error.message = `patient ${error.message}`;
          this.onGoBack();
        }
        this._toastr.error(error.message);
      });
    })
  }

  onGoBack() {
    this._router.navigate(["/allpatients"]);
  }

}
