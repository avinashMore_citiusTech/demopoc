import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientDetailsComponent } from './patient-details.component';
import { PatientService } from '../patient.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { Observable } from 'rxjs';
import { Patient } from 'src/app/model/patient';


class MockPatientService extends PatientService {
  
  private patientDetails = new Patient();
  
	getPatientDetail(patientId: any) {
		return new Observable<Patient>((observer) => {
      observer.next(this.patientDetails)
    })
	}
}
describe('PatientDetailsComponent', () => {
  let component: PatientDetailsComponent;
  let fixture: ComponentFixture<PatientDetailsComponent>;
  const mockRouter = { navigate: jasmine.createSpy('navigate') };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientDetailsComponent ],
      imports: [HttpClientModule],
      providers: [{ provide: PatientService, useClass: MockPatientService },{ provide: Router, useValue: mockRouter }, ActivatedRoute, ToastrService, HttpClient]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
