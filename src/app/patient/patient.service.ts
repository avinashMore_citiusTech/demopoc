import { Injectable } from '@angular/core';
import { Patient } from '../model/patient';
import { ApiService } from '../common-component/api/api.service';
import { Observable} from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators'

@Injectable()
export class PatientService {
  public patientResourceUrl = this.apiService.getUrl("patients");;

  constructor(
    private apiService : ApiService, 
    private httpClient: HttpClient
    ) { 
  }

  getAllPatient(patientName = null): Observable<Patient[]> {
    const patientListUrl = patientName ? `${this.patientResourceUrl}?patientName_like=${patientName}`: this.patientResourceUrl;
    return this.httpClient.get<Patient[]>(patientListUrl);
  }

  getPatientDetail(patientId): Observable<Patient> {
    return this.httpClient.get<Patient>(`${this.patientResourceUrl}/${patientId}`).pipe(
      catchError(this.apiService.handleError)
    );
  }

  updatePatient(patientId, patentDetails): Observable<Patient> {
    return this.httpClient.put<Patient>(`${this.patientResourceUrl}/${patientId}`, patentDetails);
  }

  addPatient(patientDetails): Observable<Patient> {
    return this.httpClient.post<Patient>(`${this.patientResourceUrl}`, patientDetails);
  }

  deletePatient(patient): Observable<Patient> {
    const currentDate = new Date();
    const  deletedDate = currentDate.setSeconds(currentDate.getSeconds() + 30);
    patient.deletedDate = new Date(deletedDate);
    return this.updatePatient(patient.id, patient);
  }

  deleteMulitplePatients(patients): Observable<string> {
    return Observable.create((observer: any) => {
     return patients.forEach((patient) => {
        return this.deletePatient(patient).subscribe((value) => {
         return observer.next( this.deletePatient(value));
        })
      });
    })
  }

  deletedpatientPermantly(id): Observable<any> {
    return this.httpClient.delete<any>(`${this.patientResourceUrl}/${id}`)
  }
  
  restorePatient(patient):Observable<Patient> {
    patient.deletedDate = null;
    return this.updatePatient(patient.id, patient);
  }

  getSearchData(searchText): Observable<Patient[]> {
    return this.httpClient.get<Patient[]>(`${this.patientResourceUrl}?patientName_like=${searchText}`);
  }

}
