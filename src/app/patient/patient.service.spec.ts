import { TestBed, getTestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PatientService } from './patient.service';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { ApiService } from '../common-component/api/api.service';

describe('PatientService', () => {
  let injector;
  let service: PatientService;
  let httpMock: HttpTestingController;
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, HttpClientModule],
      providers: [ApiService, HttpClient]
    });
    injector = getTestBed();
    service = injector.get(PatientService);
    httpMock = injector.get(HttpTestingController);
    expect(service).toBeTruthy();
  });
});
