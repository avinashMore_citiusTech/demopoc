import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PatientModule } from './patient/patient.module';
import { CommonComponentModule } from './common-component/common-component.module';
import { AgePipe } from './pipe/age/age.pipe';
import { AgeSortPipe } from './pipe/age-sort/age-sort.pipe';
import {HttpClientModule} from '@angular/common/http';
import { StoreModule } from '@ngrx/store';
import { reducers, metaReducers } from './reducers';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CommonComponentModule,
    AppRoutingModule,
    PatientModule,
    HttpClientModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    StoreModule.forRoot(reducers, { metaReducers })
  ],
  providers: [AgeSortPipe, AgePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
