export class Patient {
    constructor(
        public id: number = null, 
        public patientName: string = '',
        public dateOfBirth: Date = null,
        public age: Number = null,
        public deletedDate ?: Date,
        public isDelete ?: boolean
    ) {}
}
