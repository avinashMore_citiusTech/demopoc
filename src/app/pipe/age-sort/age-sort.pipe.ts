import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'ageSort'
})
export class AgeSortPipe implements PipeTransform {

  transform(array: any, args: any): any {
   return array && array.length > 0 ? array.sort((first: any, next: any) => {
      if ((first.age < next.age && (args && args.toLowerCase() === 'asc')) || (first.age > next.age && (args && args.toLowerCase() === 'desc'))) {
        return -1;
      } else if ((first.age > next.age && (args && args.toLowerCase() === 'asc')) || (first.age < next.age && (args && args.toLowerCase() === 'desc'))) {
        return 1;
      } else {
        return 0;
      }
    }) : [];
  }

}
