import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'age'
})
export class AgePipe implements PipeTransform {

  transform(value: Date, args?: any): any {
    const currentYear = new Date().getFullYear();
    const birthYear = new Date(value).getFullYear();
    return value ? (currentYear - birthYear) : null;
  }

}
