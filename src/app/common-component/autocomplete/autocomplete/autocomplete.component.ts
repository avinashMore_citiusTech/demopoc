import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit {
  @Input() dataSource: Observable<any>;
  @Output() onChnage = new EventEmitter;
  @Output() onSelect = new EventEmitter; 
  public selectedString: string;
  
  constructor() {
  }

  ngOnInit() {
  }

  changeTypeaheadLoading(e) {
    this.onChnage.emit(this.selectedString);
  }

  typeaheadOnSelect(e) {
    this.onSelect.emit(e.item);
    this.selectedString = "";
  }

}
