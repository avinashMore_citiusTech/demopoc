import { Directive, OnInit, ElementRef, Input, Inject, Renderer2 } from '@angular/core';
import { FormGroup, AbstractControl} from '@angular/forms';
import { Observable } from 'rxjs';

@Directive({
  selector: '[appValidate]'
})
export class ValidateDirective implements OnInit {
  @Input() formGroup: FormGroup;
  @Input() errorMessages: Object;
  @Input() formSubmitted: Observable<boolean>;

  public isFormSubmitted: boolean;

  constructor(private el: ElementRef, private renderer: Renderer2) {
  }
  ngOnInit() {
    this.formSubmitted.subscribe((isFormSubmit) => {
      this.isFormSubmitted = isFormSubmit;
      if(isFormSubmit) {
        this.logValidationError(this.formGroup);
      }
    })
    this.formGroup.statusChanges.subscribe(() => {
      this.logValidationError(this.formGroup)
    })
  }


  logValidationError(formGroup) {
    Object.keys(formGroup.controls).forEach((key: string) => {
      const abstractControl: AbstractControl = formGroup.get(key);
      if (abstractControl instanceof FormGroup) {
        this.logValidationError(abstractControl);
      } else {
        let parentNode;
        let errorFields;
        const errors = abstractControl.errors;
        const inputElement = this.el.nativeElement.querySelector(`[formControlName = ${key}]`);
        if (inputElement && (abstractControl.dirty || abstractControl.touched || this.isFormSubmitted)) {

          parentNode = this.renderer.parentNode(inputElement);
          const label =  parentNode.querySelector("label");
          if(label) {
            this.renderer.addClass(label, "control-label")
          }

          /**
           * Initially reset the error
           */
          errorFields = parentNode.querySelector(".help-block")
          if (errorFields) {
            this.renderer.removeChild(parentNode, errorFields);
          } 
          this.renderer.removeClass(parentNode, 'has-error');
          this.renderer.removeClass(parentNode, 'error-field');
          /**
           * end of error reset
           */

          if (abstractControl.invalid) {
            if (errors) {
              const errorElement = this.renderer.createElement('div');
              this.renderer.addClass(errorElement, 'help-block')
              this.renderer.parentNode(inputElement).appendChild(errorElement);
              this.renderer.addClass(inputElement, 'form-control');

              Object.keys(errors).forEach((errorKey) => {
                const errorMessage = this.errorMessages[key][errorKey];
                if (errorMessage) {
                    const errorSpan = this.renderer.createElement('span');
                    const errorMessageElementText = this.renderer.createText(`${errorMessage}`);
                    this.renderer.appendChild(errorSpan, errorMessageElementText);
                    this.renderer.addClass(parentNode, 'has-error');
                    this.renderer.addClass(parentNode, 'error-field');
                    this.renderer.appendChild(errorElement, errorSpan);
                }
              })
            }
          }
        }
      }
    })
  }
}
