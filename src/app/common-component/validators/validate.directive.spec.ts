import { ValidateDirective } from './validate.directive';
import {TestBed, ComponentFixture} from '@angular/core/testing';
import { Component, DebugElement, ElementRef } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  template: `<form (ngSubmit) ="onSubmitMain()" [formGroup]="testForm" appValidate [errorMessages]="errorMessages" [formSubmitted]="formSubmitted">
  <div class="form-gorup form-padding col-md-6">
    <div class="label-with-tooltip">
      <label>Name</label>
      <span class="icon-information-solid info" tooltip="Name should be minimum 3 character and maximum 30 character"
      placement="top"></span>
    </div>
    <input 
      type="text" 
      class="form-control" 
      name="patientName" 
      formControlName="patientName"
      />
  </div>
  <input type="submit" value="save" class="btn btn-primary text-uppercase">
  </form>`
})
class validateComponent {
  public formSubmitted: BehaviorSubject<boolean> = new BehaviorSubject(false);
  onSubmitMain() {
    this.formSubmitted.next(true);
  }
}

describe('ValidateDirective', () => {
  let component: validateComponent;
  let fixture: ComponentFixture<validateComponent>;
  let inputEl = DebugElement;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ValidateDirective]
    })

    fixture = TestBed.createComponent(validateComponent);
    component = fixture.componentInstance;
    inputEl = fixture.debugElement.nativeElement;
  })
  it('should create an instance', () => {
  // const directive = new ValidateDirective();
  console.log(inputEl);
  inputEl.dispatchEvent('click');
    expect(ValidateDirective).toBeTruthy();
  });
});
