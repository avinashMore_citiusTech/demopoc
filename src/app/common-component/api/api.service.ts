import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';
import { throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public baseServerUrl = "http://localhost:3000/";
  constructor() { }

  getUrl(path: string = ''): string {
    return `${this.baseServerUrl}${path}`;
  }

  handleError(errorResponse: HttpErrorResponse) {
    if(errorResponse.error instanceof ErrorEvent) {
     return throwError ({
        message: "Client side error",
        errorResponse
      })
    }

    return throwError ({
      message: _getErrorMessage(errorResponse.status),
      errorResponse
    })
    
    function _getErrorMessage(code) {
        let message = '';
        
        switch(code) {
          case 500: 
           message = 'internal server error';
           break;
          case 404: 
          message = 'not found';
          break;
          default: 
          message = 'error on server';
        }

        return message;
    }
  }
}
