import { TestBed, getTestBed } from '@angular/core/testing';

import { ModalService } from './modal.service';
import { ModalModule, BsModalService } from 'ngx-bootstrap/modal';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';

class MockModalService extends ModalService {
}

describe('ModalService', () => {
  let injector;
  let service: ModalService;
  let modalService: ModalService;
  let httpMock: HttpTestingController;
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [HttpClientTestingModule, ModalModule.forRoot()],
      providers: [BsModalService, { provide: ModalService, useValue: MockModalService }]
    })

    injector = getTestBed();
		service = injector.get(ModalService);
		httpMock = injector.get(HttpTestingController);
	//	modalService = injector.get(BsModalService);
  });

  it('should be created', () => {
   // const service: ModalService = TestBed.get(modalService);
    expect(service).toBeTruthy();
  });
});
