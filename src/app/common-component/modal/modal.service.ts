import { Injectable, TemplateRef, ComponentRef, Component, Output, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalComponent } from './modal.component';

@Injectable()
export class ModalService {
  modalRef: BsModalRef;
  config = {
    animated: true
  };
  constructor(
    private modalService: BsModalService
  ) { }

  openModal(title: string, message: string, buttons: Array<any>, template?: TemplateRef<any>) {
    this.modalRef = this.modalService.show(ModalComponent, { 
      animated: true, 
      keyboard: false, 
      backdrop: false, 
      ignoreBackdropClick: true,
      initialState: {
        title,
        message,
        buttons,
        template
      }
    });
    return new Promise<any>((resolve, reject) => this.modalService.onHide.subscribe((e) => {
      return resolve(this.modalRef.content.result);
    }))
  }
}
