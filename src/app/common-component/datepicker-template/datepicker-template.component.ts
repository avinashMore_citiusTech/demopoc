import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'app-datepicker-template',
  templateUrl: './datepicker-template.component.html',
  styleUrls: ['./datepicker-template.component.scss']
})
export class DatepickerTemplateComponent implements OnInit, OnChanges {
  @Input() parent: FormGroup;
  @Input() isDisableFeatureDate: boolean;
  @Input() selectedDate: Date;
  @Output('onChange') onDateChange = new EventEmitter();
  
  public maxDate: Date;
  public colorTheme: string = 'theme-dark-blue';

  public bsConfig: Partial<BsDatepickerConfig>;
  constructor( ) {
   
  }

  ngOnInit() {
    if(this.isDisableFeatureDate) {
      this.maxDate = new Date();
    }

    this.bsConfig = Object.assign({}, { 
      containerClass: this.colorTheme,
      dateInputFormat: 'DD/MM/YYYY'
    });
  }

  onChange(value) {
    this.onDateChange.emit(value);
  }

  ngOnChanges(changes) {
  }

}
