import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerTemplateComponent } from './datepicker-template.component';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';

describe('DatepickerTemplateComponent', () => {
  let component: DatepickerTemplateComponent;
  let fixture: ComponentFixture<DatepickerTemplateComponent>;
  const formBuilder: FormBuilder = new FormBuilder();

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatepickerTemplateComponent ],
      imports: [ReactiveFormsModule, BsDatepickerModule.forRoot()],
      providers: [{ provide: FormBuilder, useValue: formBuilder }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerTemplateComponent);
    component = fixture.componentInstance;
    component.parent = formBuilder.group({
      dateOfBirth: [null, [
        Validators.required
      ]]
    });
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
