import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { DatepickerTemplateComponent } from './datepicker-template/datepicker-template.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ModalComponent } from './modal/modal.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { ValidateDirective } from './validators/validate.directive';
import { AutocompleteComponent } from './autocomplete/autocomplete/autocomplete.component';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';

const CommonComponent = [ HeaderComponent, FooterComponent,  DatepickerTemplateComponent, ModalComponent, ValidateDirective, AutocompleteComponent];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    BsDatepickerModule.forRoot(),
    TypeaheadModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [CommonComponent],
  exports: [BsDatepickerModule, CommonComponent],
  entryComponents:[ ModalComponent ]
})
export class CommonComponentModule { }
