import { TestBed, async, inject } from '@angular/core/testing';

import { PatientAddGuard } from './patient-add.guard';

describe('PatientAddGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PatientAddGuard]
    });
  });

  it('should ...', inject([PatientAddGuard], (guard: PatientAddGuard) => {
    expect(guard).toBeTruthy();
  }));
});
