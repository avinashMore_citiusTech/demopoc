import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, Subject } from 'rxjs';
import { ModalService } from '../../common-component/modal/modal.service';
import { BsModalRef } from 'ngx-bootstrap/modal';

@Injectable()
export class PatientAddGuard implements CanActivate {
  public isRouteActivate;
  public modalRef: BsModalRef;
  public result: Subject<any> = new Subject<any>();
  
  constructor(private modal: ModalService) {
  }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      return  this.modal.openModal(
        "confirmation popup",
        "Do you really want to add patient",
        [{
        type: "button",
        value: "cancel",
        click: () => {
          this.modal.modalRef.content.result = {
            isConfirm : false
          }
          this.modal.modalRef.hide();
        }
      }, {
        type: "button",
        value: "yes",
        click: () => {
          this.modal.modalRef.content.result = {
            isConfirm : true
          }
          this.modal.modalRef.hide();
        }
      }]).then((value) => value.isConfirm);
    }
}
